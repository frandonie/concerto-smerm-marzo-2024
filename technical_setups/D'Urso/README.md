# D'Urso, Cronistoria di Un Viaggiatore Sedentario

Richieste tecniche:
- 8 uscite audio sull'ottofonia disposte ad anello (cambio palco)
- ingressi audio
  - pianoforte (2 microfoni) amplificazione trasparente
  - chitarra elettrica (1 ingresso con DI) amplificazione trasparente
  - percussioni (3/4 microfoni) amplificazione trasparente

---

Esecutori:
- Pianista (Alessandro Malcangi)
- Chitarra elettrica (Filippo Fossà)
- percussioni; Setup percussioni: tam, una coppia di bongos, rullante, cassa, ride, crash (Berardo Di Mattia)
- Fixed Media (Davide Tedesco o studente che non ha portato brano del triennio)
