# Spanedda & Sanfilippo, Waste Kompost Radio, A living forest

Richieste tecniche:
- PC
- scheda audio 4 o 8 canali (o più); minimo 96kHz samplerate
- 4 o 8 (o più) full-range near-field monitors con cono da 5 pollici minimo (Adam, M-Audio, Genelec, etc...)
- 12 cavi con mt. e connettori compatibili con lo spazio e le connessioni di altoparlanti e scheda audio
