# Scaletta del concerto

0. Installazione a cura di Luca Spanedda, Dario Sanfilippo, Alan Alpenfelt e Daniela Allocca - _Waste Kompost Radio_ con frammento per generare il materiale dell'installazione tratto da _Canto di Madre_ di Michelangelo Lupone (aprendo la sala un quarto d'ora prima del concerto)
1. Franco Evangelisti - _Incontri di Fasce Sonore_ (acusmatico), esecutore Luca Spanedda
2. Alessandro Malcangi - _Mutazioni in Clessidra_, esecutori Alessandro Malcangi (strumenti) e Luca Simone (fixed media)
3. Filippo Fossà - _La Scogliera_ (acusmatico), esecutore Filippo Fossà
4. Francesco Ferracuti - _Studio sulla mente bicamerale n.1_, esecutori Alessandro Malcangi (flauto) e Francesco Ferracuti (live electronics)
5. Francesco Vesprini - _Symphono_ (acusmatico), esecutore Francesco Vesprini
6. Davide Tedesco - _Studio sul Corpo d'Ombra \#1_, esecutori Elena D'Alò (flauto basso) e Davide Tedesco (live electronics)
7. Giulio Romano De Mattia - _AEDI - The Memory's error_, esecutori Alice Cortegiani (clarinetto basso) e Giulio Romano De Mattia (fixed media)
8. Giovanni Michelangelo D'Urso - _Cronistoria di Un Viaggiatore Sedentario_ (trascrizione per chitarra elettrica, pianoforte, percussioni e paesaggi sonori su fixed media), esescutori Francesco Bianco (chitarra elettrica), Alessandro Malcangi (pianoforte), Berardo Di Mattia (percussioni) e Edorardo Scioscia (fixed media)
9. Nicola Bernardini - _Recordare_ (acusmatico), esecutori Giulio Romano De Mattia e Nicola Bernardini
10. Installazione a cura di Luca Spanedda, Dario Sanfilippo, Alan Alpenfelt e Daniela Allocca - _Waste Kompost Radio_ dopo un'ora dall'attivazione del sistema con frammenti inseriti temporalmente nel sistema in base all'ordine della scaletta del concerto (inizia sugli applausi per 20 minuti dopo la fine del concerto)

[Link al progetto contenente la scaletta del concerto](https://www.davidetedesco.it/vault/index.php/s/WKcWzjidJ3n4yFY). Per ascoltare la scaletta scaricare [REAPER](https://www.reaper.fm/) e  scaricare tutta la cartella cliccando su "Download all files". In caso sia necessario compilare i plugin che si trovano nella cartella `used_plugins` realizzati in [FAUST](https://faust.grame.fr/) e porli dove necessario all'interno del proprio percorso dei plugin.
