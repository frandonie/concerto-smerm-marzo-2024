## Biografia

Davide Tedesco ha frequentato il corso di Musica Elettronica presso il
Conservatorio di Musica di Roma, "Santa Cecilia\", dove ha studiato con
Maestri quali Michelangelo Lupone, Nicola Bernardini, Giuseppe Silvi,
Pasquale Citera e Luigino Pizzaleo. Essi hanno svolto un ruolo cruciale
nel potenziare la sua comprensione della composizione, dell'analisi e
del pensiero musicale. Dopo aver concluso il corso di Diploma di Primo
Livello in Musica Elettronica con il massimo dei voti, Davide ha
conseguito il Diploma di Secondo Livello in Musica Elettronica presso la
stessa istituzione, ottenendo il Diploma con votazione finale di 110 su
110 e lode. Parallelamente agli studi musicali, ha iniziato a studiare
Ingegneria Informatica presso l'Università di Roma Tre, per la quale che
sta attualmente ultimando. Nell'ultimo anno, ha iniziato il corso di
specializzazione in Musica Elettronica presso l'Accademia Nazionale di
Santa Cecilia a Roma, tenuto da Michelangelo Lupone e incentrato sulle
applicazioni agli strumenti musicali aumentati.

Ha approfondito la sua formazione musicale presso l'Accademia Musicale
Chigiana a Siena, studiando, eseguendo ed interpretando brani di Berio
con i Maestri Alvise Vidolin e Nicola Bernardini. Ha partecipato a
masterclass e workshops di festival internazionali quali EMUFest ed ed
Accademia Nazionale di Santa Cecilia ed ha contribuito negli ultimi anni
al Festival ArteScienza, orgnizzato dal CRM - Centro Ricerche Musicali
nel ruolo di Assistente Musicale e Regista del suono.

Davide è Compositore di Musica Elettronica, Direttore del Suono e
interprete di Live Electronics ed ha eseguito concerti presso il
Conservatorio di Musica di Roma "Santa Cecilia\", il Goethe Institut di
Roma ed il Conservatorio di Latina "Ottorino Respighi\", collaborando
con il LEAP - Laboratorio Elettroacustico Permanente a Roma per concerti
come "Lazzaro\". Ha inoltre fondato la comunità SEAM - Sustained
ElectroAcoustic Music ed è Student Member della AES - Audio Engineering
Society dal 2021. I suoi interessi spaziano dall'Open Source alla
Sostenibilità della Musica Elettroacustica fino a tutti gli aspetti
applicativi e realizzativi della Tecnologia Musicale.

Oltre ai suoi interessi musicali, Davide ha coltivato le sue abilità
musicali studiando chitarra classica e tromba, alimentando nella vita di
tutti i giorni la sua passione per lo sport, l'arte, la tecnologia e la
natura.
